from tkinter import ttk
from tkinter import *
import tkinter as tk
from tkinter.ttk import Combobox
from PIL import Image,ImageTk
import os

myWindow = Tk()
myWindow.title("ASC Project")
myWindow.configure(background="peach puff")
frame1=Frame(height = 180,width = 1000,bg="peach puff")
frame1.pack()
l2 = Label(frame1, text="Reprezentarea si evaluarea functiilor logice",bg="coral1",fg="brown4",font="Helvetica 18 underline italic",height=1)
l2.pack()

l3 = Label(frame1, text="Alegeti numerele in baza 2", fg="coral", font="Times")
l3.pack()

lcombo= Label(frame1, bg="peach puff",height=1)
lcombo.pack()
v=["0","1"]
combo1=Combobox(frame1,values=v,width=15)
combo1.pack()
combo1.set("Primul numar")
combo1.pack()
combo2=Combobox(frame1,values=v,width=15)
combo2.set("Al doilea numar")
combo2.pack()

def clear():
    for widget in frame3.winfo_children():
        widget.destroy()

def clear_frame4():
    for widget in frame4.winfo_children():
        widget.destroy()

def clear_frame5():
    for widget in frame5.winfo_children():
        widget.destroy()

def snd1():
    os.system("C:\ProiectePython\ASC\\video1.mp4")

def new_and():
    newWindow = Toplevel(myWindow)
    newWindow.title("Invatare si aprofundare")
    newWindow.configure(background="LightPink1")
    newWindow.geometry("900x700")
    label = Label(newWindow, text="Circuite logice"
                  , bg="LightPink1", fg="Black", font="Times 18 underline italic", height=1)
    label.pack()
    frame1 = Frame(newWindow, height=400, width=1000, bg="LightPink1")
    frame1.pack()
    img = Image.open("captura_def.png")
    img = img.resize((700, 200), Image.ANTIALIAS)
    img = ImageTk.PhotoImage(img)
    panel1 = Label(frame1, image=img)
    panel1.image = img
    panel1.pack(padx=50, pady=10)
    frame2 = Frame(newWindow, height=600, width=500, bg="LightPink1")
    frame2.pack(side=LEFT, anchor=N)
    frame3 = Frame(newWindow, height=600, width=500, bg="LightPink1")
    frame3.pack(side=RIGHT, anchor=N)
    img2 = Image.open("clc.png")
    img2 = img2.resize((400, 300), Image.ANTIALIAS)
    img2 = ImageTk.PhotoImage(img2)
    panel2 = Label(frame2, image=img2)
    panel2.image = img2
    panel2.pack(padx=10, pady=20)
    img3 = Image.open("video1_img.png")
    img3 = img3.resize((400, 300), Image.ANTIALIAS)
    img3 = ImageTk.PhotoImage(img3)
    panel3 = Label(frame3, image=img3)
    panel3.image = img3
    panel3.pack(padx=10, pady=20)
    var = IntVar()
    rb1 = Radiobutton(frame3, text="Play Video", variable=var, value=1, command=snd1)
    rb1.pack(anchor=N)

def new_nand():
    newWindow = Toplevel(myWindow)
    newWindow.title("Invatare si aprofundare")
    newWindow.configure(background="LightPink1")
    label = Label(newWindow, text="Multiplexorul"
                  , bg="LightPink1", fg="Black", font="Times 15 underline italic", height=1)
    label.pack()
    img4 = Image.open("31.PNG")
    img4 = img4.resize((630, 290), Image.ANTIALIAS)
    img4 = ImageTk.PhotoImage(img4)
    panel4 = Label(newWindow, image=img4)
    panel4.image = img4
    panel4.pack(pady=5)
    label = Label(newWindow, text="Multiplexor de tip 4x1",font="Times 10 italic",bg="LightPink1", fg="Black")
    label.pack()
    label = Label(newWindow, text="6 intrări = 4 de date + 2 de selecţie şi o ieşire",
                  font="Times 10 italic",
                  bg="LightPink1", fg="Black")
    label.pack()
    img4 = Image.open("32.PNG")
    img4 = img4.resize((400, 280), Image.ANTIALIAS)
    img4 = ImageTk.PhotoImage(img4)
    panel4 = Label(newWindow, image=img4)
    panel4.image = img4
    panel4.pack(pady=2)
    newWindow.geometry("800x700")

def new_or():
    newWindow = Toplevel(myWindow)
    newWindow.title("Invatare si aprofundare")
    newWindow.configure(background="LightPink1")
    newWindow.geometry("800x700")
    label = Label(newWindow, text="Circuitul semisumator"
                  , bg="LightPink1", fg="Black", font="Times 18 underline italic", height=1)
    label.pack()
    frame2 = Frame(newWindow, height=200, width=1000, bg="LightPink1")
    frame2.pack(anchor=N)
    frame3 = Frame(newWindow, height=300, width=1000, bg="LightPink1")
    frame3.pack(anchor=N)
    frame1 = Frame(newWindow, height=200, width=1000, bg="LightPink1")
    frame1.pack()
    img2 = Image.open("semisumator.png")
    img2 = img2.resize((600, 210), Image.ANTIALIAS)
    img2 = ImageTk.PhotoImage(img2)
    panel2 = Label(frame2, image=img2)
    panel2.image = img2
    panel2.pack(pady=5)
    img3 = Image.open("semisumator2.png")
    img3 = img3.resize((600, 250), Image.ANTIALIAS)
    img3 = ImageTk.PhotoImage(img3)
    panel3 = Label(frame3, image=img3)
    panel3.image = img3
    panel3.pack(pady=5)
    course = ["0", "1"]
    l1 = Label(frame1, text="Alegeti numerele in baza 2",fg="brown1",font="Times 12 italic")
    l1.grid(column=0, row=0)
    cb = ttk.Combobox(frame1, values=course, width=10)
    cb.grid(column=0, row=2)
    cb.current(0)
    cb2 = ttk.Combobox(frame1, values=course, width=10)
    cb2.grid(column=0, row=3)
    cb2.current(0)

    def func():
        if cb.get() == cb2.get():
            s0 = 0
        else:
            s0 = 1
        if cb.get() == "1" and cb2.get() == "1":
            t0 = 1
        else:
            t0 = 0
        l2.configure(text=str(t0) + str(s0))

    b = Button(frame1, text="Rezultat",bg="salmon",font="Times 12 italic", command=func)
    b.grid(column=0, row=4)
    l2 = Label(frame1, text="", bg="LightPink1")
    l2.grid(column=0, row=7)


def new_not():
    newWindow = Toplevel(myWindow)
    newWindow.title("Invatare si aprofundare")
    newWindow.configure(background="LightPink1")
    newWindow.geometry("800x700")
    label = Label(newWindow, text="CLC semiscăzător"
                  , bg="LightPink1", fg="Black", font="Times 18 underline italic", height=1)
    label.pack()
    frame2 = Frame(newWindow, height=500, width=1000, bg="LightPink1")
    frame2.pack(anchor=N)
    frame1 = Frame(newWindow, height=200, width=1000, bg="LightPink1")
    frame1.pack()
    img2 = Image.open("semiscazator.png")
    img2 = img2.resize((700, 480), Image.ANTIALIAS)
    img2 = ImageTk.PhotoImage(img2)
    panel2 = Label(frame2, image=img2)
    panel2.image = img2
    panel2.pack(pady=5)
    course = ["0", "1"]
    l1 = Label(frame1, text="Alegeti numerele in baza 2",fg="brown1",font="Times 12 italic")
    l1.grid(column=0, row=0)
    cb = ttk.Combobox(frame1, values=course, width=10)
    cb.grid(column=0, row=2)
    cb.current(0)
    cb2 = ttk.Combobox(frame1, values=course, width=10)
    cb2.grid(column=0, row=3)
    cb2.current(0)

    def func():
        if cb.get() == cb2.get():
            d0 = 0
        else:
            d0 = 1

        if not(cb.get()) == "1" and cb2.get() == "1":
            i0 = 1
        else:
            i0 = 0
        l2.configure(text=str(d0) + str(i0))
    b = Button(frame1, text="Rezultat",bg="salmon",font="Times 12 italic", command=func)
    b.grid(column=0, row=4)
    l2 = Label(frame1, text="", bg="LightPink1")
    l2.grid(column=0, row=7)

def new_xor():
    newWindow = Toplevel(myWindow)
    newWindow.title("Invatare si aprofundare")
    newWindow.configure(background="LightPink1")
    label = Label(newWindow, text="CLC sumator complet"
                  , bg="LightPink1", fg="Black", font="Times 15 underline italic", height=1)
    label.pack()
    frameaux = Frame(newWindow, height=400, width=1000, bg="LightPink1")
    frameaux.pack()
    frame2 = Frame(frameaux, height=350, width=500, bg="LightPink1")
    frame2.pack(side=LEFT, anchor=N)
    img2 = Image.open("11.png")
    img2 = img2.resize((300, 250), Image.ANTIALIAS)
    img2 = ImageTk.PhotoImage(img2)
    panel2 = Label(frame2, image=img2)
    panel2.image = img2
    panel2.pack(pady=5, padx=20)
    frame3 = Frame(frameaux, height=350, width=500, bg="LightPink1")
    frame3.pack(side=RIGHT, anchor=N)
    img3 = Image.open("15.PNG")
    img3 = img3.resize((300, 250), Image.ANTIALIAS)
    img3 = ImageTk.PhotoImage(img3)
    panel3 = Label(frame3, image=img3)
    panel3.image = img3
    panel3.pack(pady=5, padx=20)
    frame4 = Frame(newWindow, height=305, width=800, bg="LightPink1")
    frame4.pack()
    img4 = Image.open("14.png")
    img4 = img4.resize((786, 300), Image.ANTIALIAS)
    img4 = ImageTk.PhotoImage(img4)
    panel4 = Label(frame4, image=img4)
    panel4.image = img4
    panel4.pack(pady=5)
    newWindow.geometry("900x800")
    # text1 = Text(newWindow, height=1000, width=800)
    # text1.config(state="normal")
    # text1.insert(tk.INSERT,"    Pentru simplificarea funcţiilor logice trebuiesc cunoscute un minim de identităţi din algebra  booleană: \n")
    # text1.insert(tk.INSERT, "(1) a + 0 = a (1) a 0 = 0 \n")
    # text1.insert(tk.INSERT, "(2) a + 1 = 1 (2) a 1 = a \n")
    # text1.config(state="disabled")
    # text1.pack()


def new_nxor():
    newWindow = Toplevel(myWindow)
    newWindow.title("Invatare si aprofundare")
    newWindow.configure(background="LightPink1")
    label = Label(newWindow, text="CLC scăzător complet"
                  , bg="LightPink1", fg="Black", font="Times 15 underline italic", height=1)
    label.pack()
    frameaux = Frame(newWindow, height=400, width=1000, bg="LightPink1")
    frameaux.pack()
    frame2 = Frame(frameaux, height=350, width=500, bg="LightPink1")
    frame2.pack(side=LEFT, anchor=N)
    img2 = Image.open("01.png")
    img2 = img2.resize((300, 250), Image.ANTIALIAS)
    img2 = ImageTk.PhotoImage(img2)
    panel2 = Label(frame2, image=img2)
    panel2.image = img2
    panel2.pack(pady=5, padx=20)
    frame3 = Frame(frameaux, height=350, width=500, bg="LightPink1")
    frame3.pack(side=RIGHT, anchor=N)
    img3 = Image.open("02.PNG")
    img3 = img3.resize((300, 250), Image.ANTIALIAS)
    img3 = ImageTk.PhotoImage(img3)
    panel3 = Label(frame3, image=img3)
    panel3.image = img3
    panel3.pack(pady=5, padx=20)
    frame4 = Frame(newWindow, height=305, width=800, bg="LightPink1")
    frame4.pack()
    img4 = Image.open("03.png")
    img4 = img4.resize((786, 300), Image.ANTIALIAS)
    img4 = ImageTk.PhotoImage(img4)
    panel4 = Label(frame4, image=img4)
    panel4.image = img4
    panel4.pack(pady=5)
    newWindow.geometry("900x800")

def new_nor():
    newWindow = Toplevel(myWindow)
    newWindow.title("Invatare si aprofundare")
    newWindow.configure(background="LightPink1")
    newWindow.geometry("900x700")
    label = Label(newWindow, text="Decodorul"
                  , bg="LightPink1", fg="Black", font="Times 18 underline italic", height=1)
    label.pack()
    frame1 = Frame(newWindow, height=200, width=1000, bg="LightPink1")
    frame1.pack()
    frameaux = Frame(newWindow, height=400, width=1000, bg="LightPink1")
    frameaux.pack()
    img = Image.open("decodoare1.png")
    img = img.resize((700, 200), Image.ANTIALIAS)
    img = ImageTk.PhotoImage(img)
    panel1 = Label(frame1, image=img)
    panel1.image = img
    panel1.pack(pady=5)
    frame2 = Frame(frameaux, height=250, width=400, bg="LightPink1")
    frame2.pack(side=LEFT, anchor=N)
    img2 = Image.open("decodoare2.png")
    img2 = img2.resize((400, 270), Image.ANTIALIAS)
    img2 = ImageTk.PhotoImage(img2)
    panel2 = Label(frame2, image=img2)
    panel2.image = img2
    panel2.pack(pady=5)
    frame3 = Frame(frameaux, height=250, width=400, bg="LightPink1")
    frame3.pack(side=RIGHT, anchor=N)
    img3 = Image.open("decodoare4.png")
    img3 = img3.resize((400, 270), Image.ANTIALIAS)
    img3 = ImageTk.PhotoImage(img3)
    panel3 = Label(frame3, image=img3)
    panel3.image = img3
    panel3.pack(pady=5)
    frame4 = Frame(newWindow, height=150, width=1000, bg="LightPink1")
    frame4.pack()
    img4 = Image.open("decodoare3.png")
    img4 = img4.resize((700, 100), Image.ANTIALIAS)
    img4 = ImageTk.PhotoImage(img4)
    panel4 = Label(frame4, image=img4)
    panel4.image = img4
    panel4.pack(pady=5)

def AND_GATE():
    clear()
    clear_frame4()
    clear_frame5()
    value1 = combo1.get()
    value2 = combo2.get()
    if value1 =="1" and value2 =="1":
        x=1
        img = Image.open("and11.png")
        img = img.resize((800, 250), Image.ANTIALIAS)
        img = ImageTk.PhotoImage(img)
        panel = Label(frame4, image=img)
        panel.image = img
        panel.pack(padx=50, pady=10)
    else:
        x=0
        if value1=="0" and value2=="1":
            img = Image.open("and01.png")
            img = img.resize((800, 250), Image.ANTIALIAS)
            img = ImageTk.PhotoImage(img)
            panel = Label(frame4, image=img)
            panel.image = img
            panel.pack(padx=50, pady=10)
        if value1=="1" and value2=="0":
            img = Image.open("and10.png")
            img = img.resize((800, 250), Image.ANTIALIAS)
            img = ImageTk.PhotoImage(img)
            panel = Label(frame4, image=img)
            panel.image = img
            panel.pack(padx=50, pady=10)
        if value1=="0" and value2=="0":
            img = Image.open("and00.png")
            img = img.resize((800, 250), Image.ANTIALIAS)
            img = ImageTk.PhotoImage(img)
            panel = Label(frame4, image=img)
            panel.image = img
            panel.pack(padx=50, pady=10)
    ourMessage = value1 + " AND " + value2 + " = " + str(x)
    messageVar = Message(frame3, text=ourMessage)
    messageVar.config(bg='coral', font="Times", fg="maroon", width=120)
    messageVar.pack(pady=10)
    img = Image.open("and table.png")
    img = img.resize((300, 250), Image.ANTIALIAS)
    img = ImageTk.PhotoImage(img)
    panel1 = Label(frame5, image=img)
    panel1.image = img
    panel1.pack(padx=50,pady=10)
    invatbtn = Button(frame3, text=" Invatare si aprofundare "
                      , font="Times 15", bd=12, width=20,
                      bg="salmon", command=new_and)
    invatbtn.pack(pady=10)

def NAND():
    clear()
    clear_frame4()
    clear_frame5()
    value1 = combo1.get()
    value2 = combo2.get()
    if value1 == "1" and value2 == "1":
        x1=0
        img = Image.open("nand11.png")
        img = img.resize((800, 250), Image.ANTIALIAS)
        img = ImageTk.PhotoImage(img)
        panel = Label(frame4, image=img)
        panel.image = img
        panel.pack(padx=50, pady=10)
    else:
        x1=1
        if value1=="0" and value2=="1":
            img = Image.open("nand01.png")
            img = img.resize((800, 250), Image.ANTIALIAS)
            img = ImageTk.PhotoImage(img)
            panel = Label(frame4, image=img)
            panel.image = img
            panel.pack(padx=50, pady=10)
        if value1=="1" and value2=="0":
            img = Image.open("nand10.png")
            img = img.resize((800, 250), Image.ANTIALIAS)
            img = ImageTk.PhotoImage(img)
            panel = Label(frame4, image=img)
            panel.image = img
            panel.pack(padx=50, pady=10)
        if value1=="0" and value2=="0":
            img = Image.open("nand00.png")
            img = img.resize((800, 250), Image.ANTIALIAS)
            img = ImageTk.PhotoImage(img)
            panel = Label(frame4, image=img)
            panel.image = img
            panel.pack(padx=50, pady=10)
    ourMessage1 = value1+" NAND "+value2+" = "+str(x1)
    messageVar1 = Message(frame3, text=ourMessage1)
    messageVar1.config(bg='coral', font="Times", fg="maroon",width=120)
    messageVar1.pack(pady=10)
    img = Image.open("NAND TABLE.PNG")
    img = img.resize((300, 250), Image.ANTIALIAS)
    img = ImageTk.PhotoImage(img)
    panel1 = Label(frame5, image=img)
    panel1.image = img
    panel1.pack(padx=50,pady=10)
    invatbtn = Button(frame3, text=" Invatare si aprofundare "
                      , font="Times 15", bd=12, width=20,
                      bg="salmon", command=new_nand)
    invatbtn.pack(pady=10)

def OR():
    clear()
    clear_frame4()
    clear_frame5()
    value1 = combo1.get()
    value2 = combo2.get()
    if value1 == "1" and value2=="0":
        x2=1
        img = Image.open("or10.png")
        img = img.resize((800, 250), Image.ANTIALIAS)
        img = ImageTk.PhotoImage(img)
        panel = Label(frame4, image=img)
        panel.image = img
        panel.pack(padx=50,pady=10)
    elif value1== "1" and value2=="1":
        x2=1
        img = Image.open("or11.png")
        img = img.resize((800, 250), Image.ANTIALIAS)
        img = ImageTk.PhotoImage(img)
        panel = Label(frame4, image=img)
        panel.image = img
        panel.pack(padx=50,pady=10)
    elif value1 == "0" and value2 == "1":
        x2 = 1
        img = Image.open("or01.png")
        img = img.resize((800, 250), Image.ANTIALIAS)
        img = ImageTk.PhotoImage(img)
        panel = Label(frame4, image=img)
        panel.image = img
        panel.pack(padx=50,pady=10)
    else:
        x2=0
        img = Image.open("or00.png")
        img = img.resize((800, 250), Image.ANTIALIAS)
        img = ImageTk.PhotoImage(img)
        panel = Label(frame4, image=img)
        panel.image = img
        panel.pack(padx=50,pady=10)
    ourMessage2 = value1+" OR "+value2+" = "+str(x2)
    messageVar2 = Message(frame3, text=ourMessage2)
    messageVar2.config(bg='coral', font="Times", fg="maroon",width=120)
    messageVar2.pack(pady=10)
    img = Image.open("OR TABLE.PNG")
    img = img.resize((300, 250), Image.ANTIALIAS)
    img = ImageTk.PhotoImage(img)
    panel1 = Label(frame5, image=img,)
    panel1.image = img
    panel1.pack(padx=50,pady=10)
    invatbtn = Button(frame3, text=" Invatare si aprofundare "
                      , font="Times 15", bd=12, width=20,
                      bg="salmon", command=new_or)
    invatbtn.pack(pady=10)

def NOR():
    clear()
    clear_frame4()
    clear_frame5()
    value1 = combo1.get()
    value2 = combo2.get()
    if value1=="0" and value2=="0":
        x3=1
        img = Image.open("nor00.png")
        img = img.resize((800, 250), Image.ANTIALIAS)
        img = ImageTk.PhotoImage(img)
        panel = Label(frame4, image=img)
        panel.image = img
        panel.pack(padx=50,pady=10)
    else:
        x3=0
        if value1=="0" and value2=="1":
            img = Image.open("nor01.png")
            img = img.resize((800, 250), Image.ANTIALIAS)
            img = ImageTk.PhotoImage(img)
            panel = Label(frame4, image=img)
            panel.image = img
            panel.pack(padx=50, pady=10)
        if value1=="1" and value2=="0":
            img = Image.open("nor10.png")
            img = img.resize((800, 250), Image.ANTIALIAS)
            img = ImageTk.PhotoImage(img)
            panel = Label(frame4, image=img)
            panel.image = img
            panel.pack(padx=50, pady=10)
        if value1=="1" and value2=="1":
            img = Image.open("nor11.png")
            img = img.resize((800, 250), Image.ANTIALIAS)
            img = ImageTk.PhotoImage(img)
            panel = Label(frame4, image=img)
            panel.image = img
            panel.pack(padx=50,pady=10)
    ourMessage3 = value1+" NOR "+value2+" = "+str(x3)
    messageVar3 = Message(frame3, text=ourMessage3)
    messageVar3.config(bg='coral', font="Times", fg="maroon",width=120)
    messageVar3.pack(pady=10)
    img = Image.open("NOR TABLE.PNG")
    img = img.resize((300, 250), Image.ANTIALIAS)
    img = ImageTk.PhotoImage(img)
    panel1 = Label(frame5, image=img)
    panel1.image = img
    panel1.pack(padx=50,pady=10)
    invatbtn = Button(frame3, text=" Invatare si aprofundare "
                      , font="Times 15", bd=12, width=20,
                      bg="salmon", command=new_nor)
    invatbtn.pack(pady=10)

def XOR ():
    clear()
    clear_frame4()
    clear_frame5()
    value1 = combo1.get()
    value2 = combo2.get()
    if value1 != value2:
        x4=1
        if value1=="0" and value2=="1":
            img = Image.open("xor01.png")
            img = img.resize((800, 250), Image.ANTIALIAS)
            img = ImageTk.PhotoImage(img)
            panel = Label(frame4, image=img)
            panel.image = img
            panel.pack(padx=50,pady=10)
        if value1=="1" and value2=="0":
            img = Image.open("xor10.png")
            img = img.resize((800, 250), Image.ANTIALIAS)
            img = ImageTk.PhotoImage(img)
            panel = Label(frame4, image=img)
            panel.image = img
            panel.pack(padx=50,pady=10)
    else:
        x4=0
        if value1=="0" and value2=="0":
            img = Image.open("xor00.png")
            img = img.resize((800, 250), Image.ANTIALIAS)
            img = ImageTk.PhotoImage(img)
            panel = Label(frame4, image=img)
            panel.image = img
            panel.pack(padx=50,pady=10)
        if value1=="1" and value2=="1":
            img = Image.open("xor11.png")
            img = img.resize((800, 250), Image.ANTIALIAS)
            img = ImageTk.PhotoImage(img)
            panel = Label(frame4, image=img)
            panel.image = img
            panel.pack(padx=50,pady=10)
    ourMessage4 = value1+" XOR "+value2+" = "+str(x4)
    messageVar4 = Message(frame3, text=ourMessage4)
    messageVar4.config(bg='coral', font="Times", fg="maroon",width=120)
    messageVar4.pack(pady=10)
    img = Image.open("XOR TABLE.PNG")
    img = img.resize((300, 250), Image.ANTIALIAS)
    img = ImageTk.PhotoImage(img)
    panel1 = Label(frame5, image=img)
    panel1.image = img
    panel1.pack(padx=50,pady=10)
    invatbtn = Button(frame3, text=" Invatare si aprofundare "
                      , font="Times 15", bd=12, width=20,
                      bg="salmon", command=new_xor)
    invatbtn.pack(pady=10)

def NXOR():
    clear()
    clear_frame4()
    clear_frame5()
    value1 = combo1.get()
    value2 = combo2.get()
    if value1 == value2:
        x5=1
        if value1=="0" and value2=="0":
            img = Image.open("xnor00.png")
            img = img.resize((800, 250), Image.ANTIALIAS)
            img = ImageTk.PhotoImage(img)
            panel = Label(frame4, image=img)
            panel.image = img
            panel.pack(padx=50,pady=10)
        if value1=="1" and value2=="1":
            img = Image.open("xnor11.png")
            img = img.resize((800, 250), Image.ANTIALIAS)
            img = ImageTk.PhotoImage(img)
            panel = Label(frame4, image=img)
            panel.image = img
            panel.pack(padx=50,pady=10)
    else:
        x5=0
        if value1=="0" and value2=="1":
            img = Image.open("xnor01.png")
            img = img.resize((800, 250), Image.ANTIALIAS)
            img = ImageTk.PhotoImage(img)
            panel = Label(frame4, image=img)
            panel.image = img
            panel.pack(padx=50,pady=10)
        if value1=="1" and value2=="0":
            img = Image.open("xnor10.png")
            img = img.resize((800, 250), Image.ANTIALIAS)
            img = ImageTk.PhotoImage(img)
            panel = Label(frame4, image=img)
            panel.image = img
            panel.pack(padx=50,pady=10)
    ourMessage5 = value1+" NXOR "+value2+" = "+str(x5)
    messageVar5 = Message(frame3, text=ourMessage5)
    messageVar5.config(bg='coral', font="Times", fg="maroon",width=120)
    messageVar5.pack(pady=10)
    img = Image.open("XNOR TABLE.PNG")
    img = img.resize((300, 250), Image.ANTIALIAS)
    img = ImageTk.PhotoImage(img)
    panel1 = Label(frame5, image=img)
    panel1.image = img
    panel1.pack(padx=50,pady=10)
    invatbtn = Button(frame3, text=" Invatare si aprofundare "
                      , font="Times 15", bd=12, width=20,
                      bg="salmon", command=new_nxor)
    invatbtn.pack(pady=10)

def NOT():
    clear()
    clear_frame4()
    clear_frame5()
    value1 = combo1.get()
    value2 = combo2.get()
    if value1 == "0" and value2=="0":
        val1=1
        val2=1
        img = Image.open("not00.png")
        img = img.resize((800, 250), Image.ANTIALIAS)
        img = ImageTk.PhotoImage(img)
        panel = Label(frame4, image=img)
        panel.image = img
        panel.pack(padx=50,pady=10)
    if value1 == "0" and value2=="1":
        val1=1
        val2=0
        img = Image.open("not01.png")
        img = img.resize((800, 250), Image.ANTIALIAS)
        img = ImageTk.PhotoImage(img)
        panel = Label(frame4, image=img)
        panel.image = img
        panel.pack(padx=50,pady=10)
    if value1 == "1" and value2=="0":
        val1=0
        val2=1
        img = Image.open("not10.png")
        img = img.resize((800, 250), Image.ANTIALIAS)
        img = ImageTk.PhotoImage(img)
        panel = Label(frame4, image=img)
        panel.image = img
        panel.pack(padx=50,pady=10)
    if value1 == "1" and value2=="1":
        val1=0
        val2=0
        img = Image.open("not11.png")
        img = img.resize((800, 250), Image.ANTIALIAS)
        img = ImageTk.PhotoImage(img)
        panel = Label(frame4, image=img)
        panel.image = img
        panel.pack(padx=50,pady=10)
    ourMessage6 = value1+ " NOT "+" = "+str(val1)+ "\n"+value2+" NOT "+" = "+str(val2)
    messageVar6 = Message(frame3, text=ourMessage6)
    messageVar6.config(bg='coral', font="Times", fg="maroon",width=100)
    messageVar6.pack(pady=10)
    img = Image.open("NOT TABLE.PNG")
    img = img.resize((300, 250), Image.ANTIALIAS)
    img = ImageTk.PhotoImage(img)
    panel1 = Label(frame5, image=img)
    panel1.image = img
    panel1.pack(padx=50,pady=10)
    invatbtn = Button(frame3, text=" Invatare si aprofundare "
                      , font="Times 15", bd=12, width=20,
                      bg="salmon", command=new_not)
    invatbtn.pack(pady=10)


l4 = Label(frame1, text="Alegeti operatia dorita", fg="maroon", font="Times")
l4.pack(pady=20)
frame2=Frame(height = 60,width = 1000,bg="peach puff")
frame2.pack()
andbtn=Button(frame2, text="AND", bd=5,width=18, bg="indian red",command=AND_GATE).pack(side=LEFT, expand=NO, anchor=NE, pady=2)
orbtn = Button(frame2, text="OR", bd=5 ,width=18,bg="indian red",command=OR).pack(side=LEFT, expand=YES, anchor=NE, pady=2)
notbtn=Button(frame2, text="NOT", bd=5,width=18, bg="indian red",command=NOT).pack(side=LEFT, expand=YES, anchor=NE, pady=2)
xorbtn = Button(frame2, text="XOR",bd=5,width=18, bg="indian red",command=XOR).pack(side=LEFT, expand=YES, anchor=NW, pady=2)
nxorbtn = Button(frame2, text="NXOR",bd=5,width=18, bg="indian red",command=NXOR).pack(side=LEFT, expand=YES, anchor=NW, pady=2)
norbtn = Button(frame2, text="NOR",bd=5,width=18, bg="indian red",command=NOR).pack(side=LEFT, expand=YES, anchor=NW, pady=2)
nandbtn=Button(frame2, text="NAND", bd=5 ,width=18,bg="indian red",command=NAND).pack(side=LEFT, expand=YES, anchor=NW, pady=2)

frame3=Frame(height = 300,width = 1000,bg="peach puff")
frame3.pack()
frame4=Frame(height = 300,width = 400,bg="peach puff")
frame4.pack(side=LEFT,anchor=N)
frame5=Frame(height = 300,width = 600,bg="peach puff")
frame5.pack(side=RIGHT,anchor=N)
frame6=Frame(height=900, width=600, bg="peach puff")
frame6.pack(side=BOTTOM)

myWindow.geometry("1270x650+0+0")
myWindow.mainloop()